#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright 2017-2018 Denis Meyer
#
# This file is part of the generator.
#

import abc

class CsvGenerator(object, metaclass=abc.ABCMeta):
    """Abstract base class for CSV generators"""

    @abc.abstractmethod
    def generate_csv_info(self):
        """Generates a single entry"""
        raise NotImplementedError('Method "generate_csv_info" must be implemented to use this base class.')

    @abc.abstractmethod
    def write_to_csv(self):
        """Writes everything to CSV"""
        raise NotImplementedError('Method "write_to_csv" must be implemented to use this base class.')
