#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright 2017-2018 Denis Meyer
#
# This file is part of the generator.
#

"""Generator - CoyoCsvUserGenerator"""

import os

from CsvGenerator import CsvGenerator

class CoyoCsvUserGenerator(CsvGenerator):
    """Generates users in a specific format and can store them to a CSV file"""

    users = ['Shane Rice', 'Samantha Austin', 'Sara Young', 'Harvey Garcia',
             'Jennifer Butler', 'Steven Phillips', 'Gemma Boyd', 'Aaron Coleman',
             'Lindsey Dunn', 'Tyler Hunt', 'Fiona Foster', 'Henrik Wood',
             'Madison Bailey', 'Harry King', 'Laura Morgan', 'William Foster',
             'Elizabeth Washington', 'Christopher Mitchell', 'Kelly Clarke',
             'Nicholas Hart', 'Sophie Fowler', 'Freddie Cooper']

    def __init__(self,
                 out_filename='USER_DATA',
                 companyurl='my-company.com',
                 defaultpasswd='Defaultpasswd1'):
        """Initialization

        :param out_filename: Path to the output file
        :param companyurl: The company URL
        :param defaultpasswd: The default password
        """
        self.out_filename = out_filename
        self.companyurl = companyurl
        self.defaultpasswd = defaultpasswd

    def generate_csv_info(self, name, companyurl, defaultpasswd):
        """Generates a single user information in the specified format

        :param name: The name of the user
        :param companyurl: The company URL
        :param defaultpasswd: The default password
        """
        fn, ln = name.split(' ')
        email = fn.lower() + '.' + ln.lower() + '@' + companyurl
        return ln + ',' + fn + ',' + email + ',' + defaultpasswd

    def write_to_csv(self):
        """Generates users and writes them to a csv file """
        output_path = os.getcwd() + os.sep + self.out_filename + '.csv'

        print('[INF] [usergen] Writing to {}'.format(output_path))
        try:
            with open(output_path, 'w', encoding='utf-8') as f:
                f.write('Name,Vorname,Mail,Passwort')
                for name in self.users:
                    f.write('\n' + self.generate_csv_info(name, self.companyurl, self.defaultpasswd))
            print('[INF] [usergen] Done.')
        except OSError as e:
            print('[INF] [usergen] File \'{}\' could not be opened: '.format(output_path), e)

if __name__ == '__main__':
    user_generator = CoyoCsvUserGenerator(out_filename='usergenerator-cli/example_data/USER_DATA')
    user_generator.write_to_csv()
