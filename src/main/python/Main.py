#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright 2017-2018 Denis Meyer
#
# This file is part of the COYO User Importer.
#

"""COYO User Importer - CoyoCsvUserImporter"""

#from pathlib import Path # Usage: str(Path.home()) + '/path/to/IMPORT.csv'

import os
import time
import logging
from pathlib import Path

from service.AppConfig import AppConfig
from service.coyo.CoyoData import CoyoData
from gui.GUI import GUI

# App settings
CSVSEPARATOR = ','
ARRAYSEPARATOR = '%'
OVERRIDE_GROUPDATA = True
OVERRIDE_ROLEDATA = True
row_map = None
#row_map = {
#    'email': ('', ''),
#    'loginName': ('Loginname', ''),
#    'loginNameAlt': ('', ''),
#    'firstname': ('Vorname', ''),
#    'lastname': ('Name', ''),
#    'active': ('', True),
#    'superadmin': ('', False),
#    'groupIds': ('', []),
#    'roleIds': ('', []),
#    'remoteLogonName': ('', ''),
#    'password': ('Passwort', ''),
#    'welcomeMail': ('', False),
#    'generatePassword': ('', False)
#}

# COYO settings
BASE_URL = ''
USERNAME = ''
PASSWORD = ''
CLIENTID = ''
CLIENTSECRET = ''
MULTIBACKEND = False
SESSIONNAME = ''

# Logging configuration
logging_loglevel = logging.DEBUG
logging_datefmt = '%d-%m-%Y %H:%M:%S'
logging_format = '[%(asctime)s] [%(levelname)-5s] [%(module)-20s:%(lineno)-4s] %(message)s'
logging_logfile = str(Path.home()) + '/logs/coyo.user-importer.application-' + time.strftime('%d-%m-%Y-%H-%M-%S') + '.log'

def _initialize_logger():
    """Initializes the logging utility"""
    basedir = os.path.dirname(logging_logfile)

    if not os.path.exists(basedir):
        os.makedirs(basedir)

    logging.basicConfig(level=logging_loglevel,
                        format=logging_format,
                        datefmt=logging_datefmt)

    handler_file = logging.FileHandler(logging_logfile, mode='w', encoding=None, delay=False)
    handler_file.setLevel(logging_loglevel)
    handler_file.setFormatter(logging.Formatter(fmt=logging_format, datefmt=logging_datefmt))
    logging.getLogger().addHandler(handler_file)

if __name__ == '__main__':
    _initialize_logger()

    logging.info('Starting COYO User Importer...')
    appconfig = AppConfig(csv_separator=CSVSEPARATOR,
                          array_separator=ARRAYSEPARATOR,
                          override_groupdata=OVERRIDE_GROUPDATA,
                          override_roledata=OVERRIDE_ROLEDATA,
                          row_map=row_map)
    coyodata = CoyoData(
        baseurl=BASE_URL,
        username=USERNAME,
        password=PASSWORD,
        clientid=CLIENTID,
        clientsecret=CLIENTSECRET,
        multibackend=MULTIBACKEND,
        sessionname=SESSIONNAME)
    GUI(appconfig, coyodata).display()
