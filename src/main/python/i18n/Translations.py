#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright 2017-2018 Denis Meyer
#
# This file is part of the COYO User Importer.
#

import logging

class Translations:
    """The translation table"""

    _translations = {
        'APP.AUTHOR': 'Author',
        'APP.VERSION': 'Version',
        'ASK.PRINTFAILEDUSERS': 'Print users?',
        'ASK.PRINTFAILEDUSERSLABEL': 'Print users',
        'ASK.PRINTFAILEDUSERSINFO.SINGULAR': ' user failed to be imported.',
        'ASK.PRINTFAILEDUSERSINFO.PLURAL': ' users failed to be imported.',
        'ERR.ENTER.MISSING': 'Not all input fields are valid',
        'ERR.ENTER.MISSINGLABEL': 'Invalid input',
        'ERR.ENTER.URL': 'Please enter a COYO URL',
        'ERR.ENTER.USERNAME': 'Please enter a username',
        'ERR.ENTER.PASSWORD': 'Please enter a password',
        'ERR.ENTER.CLIENTID': 'Please enter a client ID',
        'ERR.ENTER.CLIENTSECRET': 'Please enter a client secret',
        'ERR.ENTER.CSVSEPARATOR': 'Please enter a CSV separator',
        'ERR.ENTER.ARRAYSEPARATOR': 'Please enter an array (group/role IDs) separator',
        'ERR.ENTER.CSVINPUTPATH': 'Please select a CSV file',
        'FILES.ALL': 'All files',
        'FILES.CSV': 'CSV files',
        'GUI.BUTTON.BROWSECSVFILES': 'Browse a CSV file',
        'GUI.BUTTON.PROCESSING': 'Processing...',
        'GUI.BUTTON.STARTPROCESSING': 'Start processing',
        'GUI.BUTTON.QUIT': 'Quit',
        'GUI.LABEL.SETTINGS': 'General settings',
        'GUI.LABEL.MAPPINGS': 'Value mappings',
        'GUI.LABEL.OUTPUT': 'Application output',
        'GUI.LABEL.URL': 'COYO URL:',
        'GUI.LABEL.USERNAME': 'Username:',
        'GUI.LABEL.PASSWORD': 'Password:',
        'GUI.LABEL.CLIENTID': 'Client ID:',
        'GUI.LABEL.CLIENTSECRET': 'Client secret:',
        'GUI.LABEL.CSVSEPARATOR': 'CSV separator:',
        'GUI.LABEL.ARRAYSEPARATOR': 'Array (group/role IDs) separator:',
        'GUI.LABEL.MULTIBACKEND': 'Multiple backends:',
        'GUI.LABEL.SESSIONNAME': 'Session name:',
        'GUI.LABEL.OVERRIDEGROUPDATA': 'Override group data:',
        'GUI.LABEL.OVERRIDEROLEDATA': 'Override role data:',
        'GUI.LABEL.CSVFILE': 'Selected CSV file:',
        'GUI.TAB.SETTINGS': 'Settings',
        'GUI.TAB.MAPPINGS': 'Mappings',
        'GUI.TAB.OUTPUT': 'Output',
        'GUI.WINDOW.TITLE': 'COYO User Importer'
    }

    def get(self, key, default=''):
        """Returns the value for the given key or - if not found - a default value

        :param key: The key to be translated
        :param default: The default if no value could be found for the key
        """
        try:
            return self._translations[key]
        except KeyError as exception:
            logging.error('Returning default for key \'{}\': {}'.format(key, exception))
            return default
