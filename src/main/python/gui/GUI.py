#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright 2017-2018 Denis Meyer
#
# This file is part of the COYO User Importer.
#

"""COYO User Importer - GUI"""

import logging

import tkinter as tk
from tkinter import Tk
from tkinter import Menu
from tkinter.messagebox import showerror, showinfo

from i18n.Translations import Translations
from gui.MainGUI import MainGUI

class GUI(Tk):
    """The GUI for the user importer"""

    def __init__(self, appconfig, coyodata):
        """Init method

        :param appconfig: The AppConfig
        :param coyodata: The CoyoData
        """
        Tk.__init__(self)

        self.appconfig = appconfig
        self.coyodata = coyodata

        self.translations = Translations()

    def _show_about_dialog(self):
        """Displays an about dialog"""
        logging.debug('Showing about dialog')
        msg = self.appconfig.appname + '\n\n{}:\t{}\n{}:\t{}'.format(self.translations.get('APP.VERSION'),
                                                                     self.appconfig.version,
                                                                     self.translations.get('APP.AUTHOR'),
                                                                     self.appconfig.author)
        showinfo(message=msg)

    def _init_menubar(self):
        """Initializes the menu bar"""
        logging.debug('Initializing menu bar')
        self.menu = Menu(self)
        self.app_menu = Menu(self.menu, name='apple')
        self.menu.add_cascade(menu=self.app_menu)
        self.app_menu.add_command(label='About ' + self.appconfig.appname, command=self._show_about_dialog)
        self.config(menu=self.menu)

    def display(self):
        """Initializes the GUI and starts the main loop"""
        logging.debug('Showing GUI')
        self._init_menubar()

        MainGUI(self.appconfig, self.coyodata).display()
