#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright 2017-2018 Denis Meyer
#
# This file is part of the COYO User Importer.
#

"""COYO User Importer - MainGUI"""

import logging
import sys
import json
import tkinter as tk
from pathlib import Path
from tkinter import ttk, Frame, scrolledtext, NORMAL, DISABLED
from tkinter.filedialog import askopenfilename
from tkinter.messagebox import showerror

from csvtools.CoyoCsvUserImporter import CoyoCsvUserImporter
from helper.Constants import DEFAULT_ROW_MAP
from i18n.Translations import Translations
from worker.CoyoCsvUserImporterWorker import CoyoCsvUserImporterWorker

class MainGUI(Frame):
    """The GUI for the COYO User Importer"""

    _padx = 8
    _pady = 4
    _display_fullpath = True
    _display_quitbutton = False

    def __init__(self, appconfig, coyodata):
        """Initialization:

        :param appconfig: The AppConfig
        :param coyodata: The CoyoData
        """
        Frame.__init__(self)

        self.appconfig = appconfig
        self.coyodata = coyodata

        self.row_map = self.appconfig.row_map if self.appconfig.row_map else DEFAULT_ROW_MAP
        self.csv_input_path = ''
        self.translations = Translations()

    def _print(self, strtoprint):
        """Appends the given string to the output textarea

        :param strtoprint: The string to be printed
        """
        self.text_scrolled.configure(state=NORMAL)
        self.text_scrolled.insert(tk.INSERT, strtoprint + '\n')
        self.text_scrolled.configure(state=DISABLED)

    def display(self):
        """Initializes the GUI and starts the main loop"""
        logging.debug('Showing MainGUI')

        self._initgui()
        self.mainloop()

    def _doquit(self):
        """Destroys the window and quits"""
        logging.debug('Quitting...')
        self.quit()
        self.destroy()
        sys.exit(0)

    def _shorten(self, string, maxlength=50, dots='[...]'):
        """Shortens a given string

        :param string: The string to be shortened
        :param maxlength: Max string length
        :param dots: The placeholder for the shortened characters
        """
        if not isinstance(string, str) or len(string) < maxlength:
            return string
        return dots + string[-maxlength:]

    def _enable(self):
        """Enables all buttons, inputs, etc."""
        logging.debug('Enabling components')
        self.text_scrolled.configure(state=NORMAL)
        for inp in self._inputs:
            inp.config(state=NORMAL)
        for but in self._buttons:
            but.config(state=NORMAL)
        for minput in self.mappinginputs:
            minput.config(state=NORMAL)
        self.button_startprocessing['text'] = self.translations.get('GUI.BUTTON.STARTPROCESSING')

    def _disable(self):
        """Disables all buttons, inputs, etc."""
        logging.debug('Disabling components')
        self.text_scrolled.configure(state=DISABLED)
        for inp in self._inputs:
            inp.config(state=DISABLED)
        for but in self._buttons:
            but.config(state=DISABLED)
        for minput in self.mappinginputs:
            minput.config(state=DISABLED)

    def _getrowvalue(self, val):
        """Returns the converted row value

        :param val: The value
        """
        if isinstance(val, str):
            if val == '1' or val.lower() == 'true':
                return True
            elif val == '0' or val.lower() == 'false':
                return False
            elif val.startswith('['):
                return json.loads(val)
        elif isinstance(val, int):
            return False if val == 0 else True
        return str(val)

    def _getrowmap(self):
        """Returns a row map, newly constructed from the mappings tab"""
        rmap = {}
        for k, v in self.row_map.items():
            rmap[k] = (self._getrowvalue(self.mappingvalues[k].get()), self.row_map[k][1])
        return rmap

    def _startprocessing(self):
        """When the 'OK' button has been clicked"""
        logging.debug('Starting processing')
        coyourl = self.var_coyourl.get()
        username = self.var_username.get()
        password = self.var_password.get()
        clientid = self.var_clientid.get()
        clientsecret = self.var_clientsecret.get()
        multibackend = True if self.var_multibackend.get() == 1 else False
        sessionname = self.var_cookieheaderextract.get()
        csvseparator = self.var_csvseparator.get()
        arrayseparator = self.var_arrayseparator.get()
        override_groupdata = True if self.var_overridegroupdata.get() == 1 else False
        override_roledata = True if self.var_overrideroledata.get() == 1 else False
        if coyourl and username and password and clientid and clientsecret and self.csv_input_path and csvseparator and arrayseparator:
            self.tabcontrol.select(2)
            logging.info('Initializing.')

            # Application config
            self.appconfig.csv_separator = csvseparator
            self.appconfig.array_separator = arrayseparator
            self.appconfig.override_groupdata = override_groupdata
            self.appconfig.override_roledata = override_roledata
            self.appconfig.row_map = self._getrowmap()

            # COYO data
            self.coyodata.baseurl = coyourl
            self.coyodata.username = username
            self.coyodata.password = password
            self.coyodata.clientid = clientid
            self.coyodata.clientsecret = clientsecret
            self.coyodata.multibackend = multibackend
            self.coyodata.sessionname = sessionname

            userimporter = CoyoCsvUserImporter(appconfig=self.appconfig,
                                               coyodata=self.coyodata,
                                               csv_input_path=self.csv_input_path,
                                               log=self._print)
            self._disable()
            self.button_startprocessing['text'] = self.translations.get('GUI.BUTTON.PROCESSING')
            importerworker = CoyoCsvUserImporterWorker()
            importerworker.init(userimporter=userimporter, callback=self._enable)
            importerworker.start()
        else:
            errs = []
            # errs.append('True' if override_groupdata else 'False')
            # errs.append('True' if override_roledata else 'False')
            if not coyourl:
                errs.append(self.translations.get('ERR.ENTER.URL'))
            if not username:
                errs.append(self.translations.get('ERR.ENTER.USERNAME'))
            if not password:
                errs.append(self.translations.get('ERR.ENTER.PASSWORD'))
            if not clientid:
                errs.append(self.translations.get('ERR.ENTER.CLIENTID'))
            if not clientsecret:
                errs.append(self.translations.get('ERR.ENTER.CLIENTSECRET'))
            if not csvseparator:
                errs.append(self.translations.get('ERR.ENTER.CSVSEPARATOR'))
            if not arrayseparator:
                errs.append(self.translations.get('ERR.ENTER.ARRAYSEPARATOR'))
            if not self.csv_input_path or not Path(self.csv_input_path).is_file():
                errs.append(self.translations.get('ERR.ENTER.CSVINPUTPATH'))
            showerror(self.translations.get('ERR.ENTER.MISSINGLABEL'),
                      self.translations.get('ERR.ENTER.MISSING') + ':\n\n\t- ' + '\n\t- '.join(errs))
            self._enable()

    def _load_file(self):
        """When the 'Choose File' button has been clicked"""
        logging.debug('Load file')
        fname = askopenfilename(filetypes=((self.translations.get('FILES.CSV'), '*.csv'),
                                           (self.translations.get('FILES.ALL'), '*.*')))
        if fname:
            self.csv_input_path = fname
            if self._display_fullpath:
                self.var_filename.set(self._shorten(self.csv_input_path))
            else:
                path = Path(self.csv_input_path)
                self.var_filename.set(self._shorten(path.name))

    def _center(self):
        """Centers the window"""
        logging.debug('Centering application')
        self.master.update_idletasks()
        width = self.master.winfo_width()
        frm_width = self.master.winfo_rootx() - self.master.winfo_x()
        win_width = width + 2 * frm_width
        height = self.master.winfo_height()
        titlebar_height = self.master.winfo_rooty() - self.master.winfo_y()
        win_height = height + titlebar_height + frm_width
        x = self.master.winfo_screenwidth() // 2 - win_width // 2
        y = self.master.winfo_screenheight() // 2 - win_height // 2
        self.master.geometry('{}x{}+{}+{}'.format(width, height, x, y))
        self.master.resizable(width=False, height=False)
        self.master.deiconify()

    def _addtogrid(self):
        """Adds the UI components to the grid"""
        logging.debug('Adding components to grid')
        currrow = 0
        currcolumn = 0
        rowspan = 1
        colspan = 1
        self.tabcontrol.grid(row=currrow,
                             column=currcolumn,
                             rowspan=rowspan,
                             columnspan=colspan,
                             sticky=tk.N+tk.E+tk.S+tk.W)
        currrow += 1
        self.button_startprocessing.grid(row=currrow,
                                         column=currcolumn,
                                         rowspan=rowspan,
                                         columnspan=colspan,
                                         sticky=tk.N+tk.E+tk.S+tk.W)
        currrow += 1
        if self._display_quitbutton:
            self.button_quit.grid(row=currrow,
                                  column=currcolumn,
                                  rowspan=rowspan,
                                  columnspan=colspan,
                                  sticky=tk.N+tk.E+tk.S+tk.W)

        sticky = tk.N+tk.E+tk.W
        self.labelframe1.grid(column=0,
                              row=0,
                              padx=self._padx,
                              pady=self._pady,
                              sticky=sticky)
        self.labelframe2.grid(column=0,
                              row=0,
                              padx=self._padx,
                              pady=self._pady,
                              sticky=sticky)
        self.labelframe3.grid(column=0,
                              row=0,
                              padx=self._padx,
                              pady=self._pady,
                              sticky=sticky+tk.S)

        currrow = 0
        currcolumn = 0
        rowspan = 1
        colspan = 1
        colspaninput = 5
        sticky = tk.N+tk.E+tk.S+tk.W
        self.label_coyourl.grid(row=currrow,
                                column=currcolumn,
                                rowspan=rowspan,
                                columnspan=colspan,
                                sticky=sticky)
        self.input_coyourl.grid(row=currrow,
                                column=currcolumn+1,
                                rowspan=rowspan,
                                columnspan=colspaninput,
                                sticky=sticky)
        currrow += 1
        self.label_username.grid(row=currrow,
                                 column=currcolumn,
                                 rowspan=rowspan,
                                 columnspan=colspan,
                                 sticky=sticky)
        self.input_username.grid(row=currrow,
                                 column=currcolumn+1,
                                 rowspan=rowspan,
                                 columnspan=colspaninput,
                                 sticky=sticky)
        currrow += 1
        self.label_password.grid(row=currrow,
                                 column=currcolumn,
                                 rowspan=rowspan,
                                 columnspan=colspan,
                                 sticky=sticky)
        self.input_password.grid(row=currrow,
                                 column=currcolumn+1,
                                 rowspan=rowspan,
                                 columnspan=colspaninput,
                                 sticky=sticky)
        currrow += 1
        self.label_clientid.grid(row=currrow,
                                 column=currcolumn,
                                 rowspan=rowspan,
                                 columnspan=colspan,
                                 sticky=sticky)
        self.input_clientid.grid(row=currrow,
                                 column=currcolumn+1,
                                 rowspan=rowspan,
                                 columnspan=colspaninput,
                                 sticky=sticky)
        currrow += 1
        self.label_clientsecret.grid(row=currrow,
                                     column=currcolumn,
                                     rowspan=rowspan,
                                     columnspan=colspan,
                                     sticky=sticky)
        self.input_clientsecret.grid(row=currrow,
                                     column=currcolumn+1,
                                     rowspan=rowspan,
                                     columnspan=colspaninput,
                                     sticky=sticky)
        currrow += 1
        self.label_arrayseparator.grid(row=currrow,
                                       column=currcolumn,
                                       rowspan=rowspan,
                                       columnspan=colspan,
                                       sticky=sticky)
        self.input_arrayseparator.grid(row=currrow,
                                       column=currcolumn+1,
                                       rowspan=rowspan,
                                       columnspan=colspaninput,
                                       sticky=sticky)
        currrow += 1
        self.label_multibackend.grid(row=currrow,
                                     column=currcolumn,
                                     rowspan=rowspan,
                                     columnspan=colspan,
                                     sticky=sticky)
        self.checkbutton_multibackend.grid(row=currrow,
                                           column=currcolumn+1,
                                           rowspan=rowspan,
                                           columnspan=colspaninput,
                                           sticky=sticky)
        currrow += 1
        self.label_cookieheaderextract.grid(row=currrow,
                                            column=currcolumn,
                                            rowspan=rowspan,
                                            columnspan=colspan,
                                            sticky=sticky)
        self.input_cookieheaderextract.grid(row=currrow,
                                            column=currcolumn+1,
                                            rowspan=rowspan,
                                            columnspan=colspaninput,
                                            sticky=sticky)
        currrow += 1
        self.label_override_groupdata.grid(row=currrow,
                                           column=currcolumn,
                                           rowspan=rowspan,
                                           columnspan=colspan,
                                           sticky=sticky)
        self.checkbutton_override_groupdata.grid(row=currrow,
                                                 column=currcolumn+1,
                                                 rowspan=rowspan,
                                                 columnspan=colspaninput,
                                                 sticky=sticky)
        currrow += 1
        self.label_override_roledata.grid(row=currrow,
                                          column=currcolumn,
                                          rowspan=rowspan,
                                          columnspan=colspan,
                                          sticky=sticky)
        self.checkbutton_override_roledata.grid(row=currrow,
                                                column=currcolumn+1,
                                                rowspan=rowspan,
                                                columnspan=colspaninput,
                                                sticky=sticky)
        currrow += 1
        self.label_csvseparator.grid(row=currrow,
                                     column=currcolumn,
                                     rowspan=rowspan,
                                     columnspan=colspan,
                                     sticky=sticky)
        self.input_csvseparator.grid(row=currrow,
                                     column=currcolumn+1,
                                     rowspan=rowspan,
                                     columnspan=colspaninput,
                                     sticky=sticky)
        currrow += 1
        self.label_fileselect.grid(row=currrow,
                                   column=currcolumn,
                                   rowspan=rowspan,
                                   columnspan=colspan,
                                   sticky=sticky)
        self.label_filename.grid(row=currrow,
                                 column=currcolumn+1,
                                 rowspan=rowspan,
                                 columnspan=colspaninput,
                                 sticky=sticky)
        currrow += 1
        self.button_choosefile.grid(row=currrow,
                                    column=currcolumn+1,
                                    rowspan=rowspan,
                                    columnspan=colspaninput,
                                    sticky=sticky)

        currrow = 0
        currcolumn = 0
        rowspan = 1
        colspan = 1
        colspaninput = 5
        sticky = tk.N+tk.E+tk.W
        for l, i in zip(self.mappinglabels, self.mappinginputs):
            l.grid(row=currrow,
                   column=currcolumn,
                   rowspan=rowspan,
                   columnspan=colspan,
                   sticky=sticky)
            i.grid(row=currrow,
                   column=currcolumn+1,
                   rowspan=rowspan,
                   columnspan=colspaninput,
                   sticky=sticky)
            currrow += 1

        currrow = 0
        currcolumn = 0
        rowspan = 3
        colspan = 3
        sticky = tk.N+tk.E+tk.S+tk.W
        self.text_scrolled.grid(row=currrow,
                                column=currcolumn,
                                rowspan=rowspan,
                                columnspan=colspan,
                                sticky=sticky)

    def _init_scrolledtext(self):
        """Initializes the scrolledtext"""
        logging.debug('Initializing scrolled text')
        scrol_w = 100
        scrol_h = 20
        self.text_scrolled = scrolledtext.ScrolledText(self.labelframe3,
                                                       width=scrol_w,
                                                       height=scrol_h,
                                                       wrap=tk.WORD)
        self.text_scrolled.configure(state="disabled")

    def _init_buttons(self):
        """Initializes the buttons"""
        logging.debug('Initializing buttons')
        self._buttons = []
        self.button_choosefile = ttk.Button(self.labelframe1,
                                            text=self.translations.get('GUI.BUTTON.BROWSECSVFILES'),
                                            command=self._load_file)
        self._buttons.append(self.button_choosefile)
        self.button_startprocessing = ttk.Button(self,
                                                 text=self.translations.get('GUI.BUTTON.STARTPROCESSING'),
                                                 command=self._startprocessing)
        self._buttons.append(self.button_startprocessing)
        if self._display_quitbutton:
            self.button_quit = ttk.Button(self,
                                          text=self.translations.get('GUI.BUTTON.QUIT'),
                                          command=self._doquit)
            self._buttons.append(self.button_quit)

    def _init_inputs(self):
        """Initializes the inputs"""
        logging.debug('Initializing inputs')
        self._inputs = []
        self.input_coyourl = ttk.Entry(self.labelframe1,
                                       width=12,
                                       textvariable=self.var_coyourl)
        self._inputs.append(self.input_coyourl)
        self.input_username = ttk.Entry(self.labelframe1,
                                        width=12,
                                        textvariable=self.var_username)
        self._inputs.append(self.input_username)
        self.input_password = ttk.Entry(self.labelframe1,
                                        width=12,
                                        textvariable=self.var_password)
        self._inputs.append(self.input_password)
        self.input_clientid = ttk.Entry(self.labelframe1,
                                        width=12,
                                        textvariable=self.var_clientid)
        self._inputs.append(self.input_clientid)
        self.input_clientsecret = ttk.Entry(self.labelframe1,
                                            width=12,
                                            textvariable=self.var_clientsecret)
        self._inputs.append(self.input_clientsecret)
        self.input_arrayseparator = ttk.Entry(self.labelframe1,
                                              width=12,
                                              textvariable=self.var_arrayseparator)
        self._inputs.append(self.input_arrayseparator)
        self.checkbutton_multibackend = ttk.Checkbutton(self.labelframe1,
                                                        text='',
                                                        variable=self.var_multibackend)
        self._inputs.append(self.checkbutton_multibackend)
        self.input_cookieheaderextract = ttk.Entry(self.labelframe1,
                                                   width=12,
                                                   textvariable=self.var_cookieheaderextract)
        self._inputs.append(self.input_cookieheaderextract)
        self.checkbutton_override_groupdata = ttk.Checkbutton(self.labelframe1,
                                                              text='',
                                                              variable=self.var_overridegroupdata)
        self._inputs.append(self.checkbutton_override_groupdata)
        self.checkbutton_override_roledata = ttk.Checkbutton(self.labelframe1,
                                                             text='',
                                                             variable=self.var_overrideroledata)
        self._inputs.append(self.checkbutton_override_roledata)
        self.input_csvseparator = ttk.Entry(self.labelframe1,
                                            width=12,
                                            textvariable=self.var_csvseparator)
        self._inputs.append(self.input_csvseparator)

        self.mappingvalues = {}
        self.mappinginputs = []
        for k, v in self.row_map.items():
            self.mappingvalues[k] = tk.StringVar()
            self.mappingvalues[k].set(v[0])
            self.mappinginputs.append(ttk.Entry(self.labelframe2,
                                                width=50,
                                                text=self.mappingvalues[k]))

    def _init_labels(self):
        """Initializes the labels"""
        logging.debug('Initializing labels')
        self.label_coyourl = ttk.Label(self.labelframe1,
                                       text=self.translations.get('GUI.LABEL.URL'))
        self.label_username = ttk.Label(self.labelframe1,
                                        text=self.translations.get('GUI.LABEL.USERNAME'))
        self.label_password = ttk.Label(self.labelframe1,
                                        text=self.translations.get('GUI.LABEL.PASSWORD'))
        self.label_clientid = ttk.Label(self.labelframe1,
                                        text=self.translations.get('GUI.LABEL.CLIENTID'))
        self.label_clientsecret = ttk.Label(self.labelframe1,
                                            text=self.translations.get('GUI.LABEL.CLIENTSECRET'))
        self.label_csvseparator = ttk.Label(self.labelframe1,
                                            text=self.translations.get('GUI.LABEL.CSVSEPARATOR'))
        self.label_arrayseparator = ttk.Label(self.labelframe1,
                                              text=self.translations.get('GUI.LABEL.ARRAYSEPARATOR'))
        self.label_multibackend = ttk.Label(self.labelframe1,
                                            text=self.translations.get('GUI.LABEL.MULTIBACKEND'))
        self.label_cookieheaderextract = ttk.Label(self.labelframe1,
                                                   text=self.translations.get('GUI.LABEL.SESSIONNAME'))
        self.label_override_groupdata = ttk.Label(self.labelframe1,
                                                  text=self.translations.get('GUI.LABEL.OVERRIDEGROUPDATA'))
        self.label_override_roledata = ttk.Label(self.labelframe1,
                                                 text=self.translations.get('GUI.LABEL.OVERRIDEROLEDATA'))
        self.label_fileselect = ttk.Label(self.labelframe1,
                                          text=self.translations.get('GUI.LABEL.CSVFILE'))
        self.label_filename = ttk.Label(self.labelframe1,
                                        textvariable=self.var_filename)
        self.mappinglabels = []
        for k, v in self.row_map.items():
            self.mappinglabels.append(ttk.Label(self.labelframe2, text=k))

    def _init_tabs(self):
        """Initializes the UI tabs"""
        logging.debug('Initializing tabs')
        self.tabcontrol = ttk.Notebook(self)
        self.tab1 = ttk.Frame(self.tabcontrol)
        self.tab2 = ttk.Frame(self.tabcontrol)
        self.tab3 = ttk.Frame(self.tabcontrol)
        self.tabcontrol.add(self.tab1,
                            text=self.translations.get('GUI.TAB.SETTINGS'))
        self.tabcontrol.add(self.tab2,
                            text=self.translations.get('GUI.TAB.MAPPINGS'))
        self.tabcontrol.add(self.tab3,
                            text=self.translations.get('GUI.TAB.OUTPUT'))
        self.labelframe1 = ttk.LabelFrame(self.tab1,
                                          text=self.translations.get('GUI.LABEL.SETTINGS'))
        self.labelframe2 = ttk.LabelFrame(self.tab2,
                                          text=self.translations.get('GUI.LABEL.MAPPINGS'))
        self.labelframe3 = ttk.LabelFrame(self.tab3,
                                          text=self.translations.get('GUI.LABEL.OUTPUT'))

        self.tabcontrol.rowconfigure(0, weight=1)
        self.tabcontrol.columnconfigure(0, weight=1)
        self.tab1.rowconfigure(0, weight=1)
        self.tab1.columnconfigure(0, weight=1)
        self.tab2.rowconfigure(0, weight=1)
        self.tab2.columnconfigure(0, weight=1)
        self.tab3.rowconfigure(0, weight=1)
        self.tab3.columnconfigure(0, weight=1)

        self.labelframe1.rowconfigure(0, weight=1)
        self.labelframe1.rowconfigure(1, weight=1)
        self.labelframe1.columnconfigure(0, weight=1)
        self.labelframe1.columnconfigure(1, weight=4)
        self.labelframe2.rowconfigure(0, weight=1)
        self.labelframe2.rowconfigure(1, weight=1)
        self.labelframe2.columnconfigure(0, weight=1)
        self.labelframe2.columnconfigure(1, weight=4)
        self.labelframe3.rowconfigure(0, weight=1)
        self.labelframe3.rowconfigure(1, weight=1)
        self.labelframe3.columnconfigure(0, weight=1)
        self.labelframe3.columnconfigure(1, weight=1)

    def _inittkvariables(self):
        """Initializes Tkinter variables for the inputs, etc."""
        logging.debug('Initializing tk variables')
        self.var_coyourl = tk.StringVar()
        self.var_coyourl.set(self.coyodata.baseurl)
        self.var_username = tk.StringVar()
        self.var_username.set(self.coyodata.username)
        self.var_password = tk.StringVar()
        self.var_password.set(self.coyodata.password)
        self.var_clientid = tk.StringVar()
        self.var_clientid.set(self.coyodata.clientid)
        self.var_clientsecret = tk.StringVar()
        self.var_clientsecret.set(self.coyodata.clientsecret)
        self.var_arrayseparator = tk.StringVar()
        self.var_arrayseparator.set(self.appconfig.array_separator)
        self.var_multibackend = tk.IntVar()
        self.var_multibackend.set(1 if self.coyodata.multibackend else 0)
        self.var_cookieheaderextract = tk.StringVar()
        self.var_cookieheaderextract.set(self.coyodata.sessionname)
        self.var_overridegroupdata = tk.IntVar()
        self.var_overridegroupdata.set(1 if self.appconfig.override_groupdata else 0)
        self.var_overrideroledata = tk.IntVar()
        self.var_overrideroledata.set(1 if self.appconfig.override_roledata else 0)
        self.var_csvseparator = tk.StringVar()
        self.var_csvseparator.set(self.appconfig.csv_separator)
        self.var_filename = tk.StringVar()
        self.var_filename.set(self._shorten(self.csv_input_path))

    def _configureproperties(self):
        """Configures frame properties"""
        logging.debug('Configuring properties')
        self.master.title(self.translations.get('GUI.WINDOW.TITLE'))
        self.master.rowconfigure(0, weight=1)
        self.master.columnconfigure(0, weight=1)
        self.grid(sticky=tk.E+tk.S+tk.W, padx=self._padx, pady=self._pady)

    def _registercommands(self):
        """Registers some commands on special events, e.g. 'exit'"""
        logging.debug('Registering commands')
        self.master.createcommand('exit', self._doquit)

    def _initgui(self):
        """Initializes the GUI"""
        logging.debug('Initializing GUI')
        self._registercommands()
        self._configureproperties()
        self._inittkvariables()
        self._init_tabs()
        self._init_labels()
        self._init_inputs()
        self._init_buttons()
        self._init_scrolledtext()
        self._addtogrid()
        self._center()
        self._enable()
