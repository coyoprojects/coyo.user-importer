#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright 2017-2018 Denis Meyer
#
# This file is part of the COYO User Importer.
#

DEFAULT_ROW_MAP = {
    'email': ('email', ''),
    'loginName': ('loginName', ''),
    'loginNameAlt': ('loginNameAlt', ''),
    'firstname': ('firstname', ''),
    'lastname': ('lastname', ''),
    'active': ('active', True),
    'superadmin': ('superadmin', False),
    'groupIds': ('groupIds', []),
    'roleIds': ('roleIds', []),
    'remoteLogonName': ('remoteLogonName', ''),
    'password': ('password', ''),
    'welcomeMail': ('welcomeMail', False),
    'generatePassword': ('generatePassword', False)
}
