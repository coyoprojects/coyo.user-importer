#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright 2017-2018 Denis Meyer
#
# This file is part of the COYO User Importer.
#

"""COYO User Importer - CoyoCsvUserImporterWorker"""

import logging
import threading

from tkinter.messagebox import askokcancel

from csvtools.CoyoCsvUserImporter import CoyoCsvUserImporter
from i18n.Translations import Translations

class CoyoCsvUserImporterWorker(threading.Thread):
    """The COYO User Importer background worker thread"""

    def init(self,
             userimporter=None,
             callback=None):
        """Initializes default values because __init__ is not available for threads.

        :param userimporter: The user importer instance
        :param logger: The logger
        :param callback: The callback function
        """
        assert isinstance(userimporter, CoyoCsvUserImporter)
        self.userimporter = userimporter
        self.callback = callback
        self.translations = Translations()

    def run(self):
        """Starts the thread"""
        try:
            self.userimporter.importusers()

            if self.userimporter.processed:
                logging.info('Successfully processed users.')
                self.userimporter.print_failedusers_info()
                flen = len(self.userimporter.failedusers)
                infstr = str(flen) + (self.translations.get('ASK.PRINTFAILEDUSERSINFO.SINGULAR' if flen == 1 else 'ASK.PRINTFAILEDUSERSINFO.PLURAL'))
                if self.userimporter.failedusers and askokcancel(self.translations.get('ASK.PRINTFAILEDUSERSLABEL'), infstr + '\n\n' + self.translations.get('ASK.PRINTFAILEDUSERS')):
                    self.userimporter.print_failedusers()
            else:
                logging.info('Did not process.')
        except Exception as ex:
            logging.exception(ex)
        finally:
            if self.callback:
                self.callback()
