#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright 2017-2018 Denis Meyer
#
# This file is part of the COYO User Importer.
#

"""COYO User Importer - CoyoOps"""

import logging
import json
import base64
import re
from urllib.request import Request, urlopen
from urllib.parse import quote_plus, urlencode
from urllib.error import HTTPError

def _get_displayname(firstname, lastname):
    """Returns the display name

    :param firstname: The first name
    :param lastname: The last name
    """
    return firstname + ' ' + lastname

class CoyoOps:
    """Authenticates with COYO and performs requests"""

    _urls = {
        'token': '/api/oauth/token',
        'groups': '/api/groups',
        'roles': '/api/roles',
        'users': '/api/users'
    }
    _granttype = 'grant_type=password'
    _acctoken = 'access_token='
    _displayName = 'displayName='
    _withAdminFields = 'with=adminFields'
    _page = '_page=0&_pageSize=1000'

    # Set override_groupdata to False if groups that a user already has should not be overridden
    # Set override_roledata to False if roles that a user already has should not be overridden
    def __init__(self,
                 appconfig,
                 coyodata):
        """Initialization

        :param appconfig: The AppConfig
        :param coyodata: The CoyoData
        """
        self.appconfig = appconfig
        self.coyodata = coyodata

        self.oauthdata = {}
        self.accesstoken = ''
        self.valid = False
        self.users = {}
        self.roles = {}
        self.groups = {}
        self._additionalheaders = {}

    def init(self):
        """Initializes the Ops"""
        logging.debug('Initializing')
        self._get_accesstoken()
        self._findandparse_allgroups()
        self._findandparse_allroles()
        self._findandparse_allusers()

    def createorupdate_user(self, data_dict):
        """Creates or updates a new user in COYO

        :param data_dict: The data as a dictionary
        """
        if data_dict['email'] and data_dict['email'] in self.users:
            try:
                logging.debug('User already existing. Updating user with id \'{}\'...'
                    .format(self.users[data_dict['email']]['id']))
                return self._update_user(data_dict, self.users[data_dict['email']])
            except Exception:
                logging.error('Could not find user in existing users. Trying to get user via find.')
                parsedjson = self._findandparse_user(data_dict)
                if parsedjson:
                    logging.info('User already existing. Updating user with id \'{}\'...'
                                    .format(parsedjson['id']))
                    return self._update_user(data_dict, parsedjson)
        logging.info('User not existing. Creating user.')
        return self._create_user(data_dict)

    def _get_gettoken_url(self):
        """Returns the 'get token' URL"""
        #unamep = '&username=' + self.credentials.username + '&password=' + self.credentials.password
        return self.coyodata.baseurl + self._urls['token'] + '?' + self._granttype # + unamep

    def _get_creategroup_url(self):
        """Returns the 'create group' URL"""
        return self.coyodata.baseurl + self._urls['groups'] + '?' + self._acctoken + self.accesstoken

    def _get_createrole_url(self):
        """Returns the 'create role' URL"""
        return self.coyodata.baseurl + self._urls['roles'] + '?' + self._acctoken + self.accesstoken

    def _get_createuser_url(self):
        """Returns the 'create user' URL"""
        return self.coyodata.baseurl + self._urls['users'] + '?' + self._acctoken + self.accesstoken

    def _get_findallgroups_url(self):
        """Returns the 'find all groups' URL"""
        return self.coyodata.baseurl + self._urls['groups'] + '?' + self._page + '&' + self._acctoken + self.accesstoken

    def _get_findallroles_url(self):
        """Returns the 'find all roles' URL"""
        return self.coyodata.baseurl + self._urls['roles'] + '?' + self._page + '&' + self._acctoken + self.accesstoken

    def _get_findallusers_url(self):
        """Returns the 'find all users' URL"""
        return self.coyodata.baseurl + self._urls['users'] + '?' + self._withAdminFields + '&' + self._page + '&' + self._acctoken + self.accesstoken

    def _get_finduser_url(self, displayname):
        """Returns the 'find user' URL

        :param displayname: The display name
        """
        return self.coyodata.baseurl + self._urls['users'] + '?' + self._withAdminFields + '&' + self._page + '&' + self._displayName + quote_plus(displayname) + '&' + self._acctoken + self.accesstoken

    def _get_updateuser_url(self, userid):
        """Returns the 'update user' URL

        :param userid: The user ID
        """
        return self.coyodata.baseurl + self._urls['users'] + '/' + userid + '?' + self._acctoken + self.accesstoken

    def _extractheaders(self, request):
        """Extracts request headers

        :param request: The request
        """
        if self.coyodata.multibackend:
            if self.coyodata.sessionname:
                logging.debug('Extracting cookie header...')
                header_setcookie = request.getheader('Set-Cookie')
                extracted = False
                if header_setcookie:
                    regex = r'' + self.coyodata.sessionname + '=(.*);'
                    matches = re.findall(regex, header_setcookie)
                    if matches:
                        self._additionalheaders['Cookie'] = self.coyodata.sessionname + '=' + matches[0]
                        extracted = True
                if not extracted:
                    self._additionalheaders['Cookie'] = None
            else:
                self._additionalheaders['Cookie'] = None
                logging.error('Could not extract cookie header. Set the correct header "Set-Cookie" from the request:')
                logging.error(request.info())

    def _retrieve_oauthdata(self):
        """Retrieves the OAuth2 data"""
        logging.debug('Retrieving OAuth2 data.')
        post_fields = {
            'username': self.coyodata.username,
            'password': self.coyodata.password
        }
        base64string = base64.b64encode('{}:{}'
                                        .format(self.coyodata.clientid, self.coyodata.clientsecret).encode('utf-8'))
        base64string_stripped = str(base64string)[2:-1]
        try:
            req = Request(self._get_gettoken_url())
            if self._additionalheaders:
                for key in self._additionalheaders:
                    req.add_header(key, self._additionalheaders[key])
            req.add_header('Authorization', 'Basic {}'.format(base64string_stripped))
            with urlopen(req, bytes(urlencode(post_fields).encode())) as request:
                self._extractheaders(request)
                logging.debug('Successfully retrieved OAuth2 data.')
                decoded = request.read().decode('utf-8')
                self.oauthdata = json.loads(decoded)
                return True
        except HTTPError as exception:
            logging.error('Could not retrieve access token: {}'.format(exception))
            self.valid = False

    def _get_accesstoken(self):
        """Retrieves the access token"""
        self.valid = False
        self.accesstoken = None
        if self._retrieve_oauthdata():
            logging.debug('Extracting access token.')
            try:
                self.accesstoken = self.oauthdata['access_token']
                self.valid = True
                logging.debug('Successfully extracted access token.')
            except KeyError as exception:
                logging.error('Could not extract access token: {}'.format(exception))
        return self.accesstoken

    def _findandparse_allgroups(self):
        """Finds all groups and parses them into a dict (id -> jsonData)"""
        logging.debug('Retrieving all groups data.')
        try:
            response = self._findall_groups()
            if response[0]:
                parsedjson = json.loads(response[1])['content']
                for group in parsedjson:
                    self.groups[group['id']] = group
        except KeyError:
            logging.error('Could not find any groups.')
            self.groups = {}
        logging.debug('Found {} groups'.format(len(self.groups)))

    def _findandparse_allroles(self):
        """Finds all roles and parses them into a dict (id -> jsonData)"""
        logging.debug('Retrieving all roles data.')
        try:
            response = self._findall_roles()
            if response[0]:
                parsedjson = json.loads(response[1])['content']
                for role in parsedjson:
                    self.roles[role['id']] = role
        except KeyError:
            logging.error('Could not find any roles.')
            self.roles = {}
        logging.debug('Found {} roles'.format(len(self.roles)))

    def _findandparse_allusers(self):
        """Finds all users and parses them into a dict (email -> jsonData)"""
        logging.debug('Retrieving all users data.')
        try:
            response = self._findall_users()
            if response[0]:
                parsedjson = json.loads(response[1])['content']
                for user in parsedjson:
                    self.users[user['email']] = user
        except KeyError:
            logging.error('Could not find any users.')
            self.users = {}
        logging.debug('Found {} users'.format(len(self.users)))

    def _findandparse_user(self, data_dict):
        """Finds a specific user

        :param data_dict: The data as a dictionary
        """
        parsedjson = True
        try:
            response = self._find_user(_get_displayname(data_dict['firstname'], data_dict['lastname']))
            if response[0]:
                try:
                    parsedjson = json.loads(response[1])['content']
                    for user in parsedjson:
                        if user['email'] == data_dict['email']:
                            return user
                except KeyError:
                    parsedjson = False
        except KeyError:
            parsedjson = False
        if parsedjson:
            parsedjson = json.loads(response[1])['content'][0]
        return parsedjson

    def _findall_groups(self):
        """Finds all groups"""
        logging.debug('Find all groups')
        try:
            req = Request(self._get_findallgroups_url(), method='GET')
            for key in self._additionalheaders:
                value = self._additionalheaders[key]
                if value:
                    req.add_header(key, value)
            with urlopen(req) as request:
                return (True, request.read().decode('utf-8'))
        except HTTPError as exception:
            return (False, exception)

    def _findall_roles(self):
        """Finds all roles"""
        logging.debug('Find all roles')
        try:
            req = Request(self._get_findallroles_url(), method='GET')
            for key in self._additionalheaders:
                value = self._additionalheaders[key]
                if value:
                    req.add_header(key, value)
            with urlopen(req) as request:
                return (True, request.read().decode('utf-8'))
        except HTTPError as exception:
            return (False, exception)

    def _findall_users(self):
        """Finds all users"""
        logging.debug('Find all users')
        try:
            req = Request(self._get_findallusers_url(), method='GET')
            for key in self._additionalheaders:
                value = self._additionalheaders[key]
                if value:
                    req.add_header(key, value)
            with urlopen(req) as request:
                return (True, request.read().decode('utf-8'))
        except HTTPError as exception:
            return (False, exception)

    def _find_user(self, displayname):
        """Finds a user

        :param displayname: The display name
        """
        try:
            req = Request(self._get_finduser_url(displayname), method='GET')
            for key in self._additionalheaders:
                value = self._additionalheaders[key]
                if value:
                    req.add_header(key, value)
            with urlopen(req) as request:
                return (True, request.read().decode('utf-8'))
        except HTTPError as exception:
            return (False, exception)

    def _create_group(self, displayname):
        """Creates a new group in COYO

        :param displayname: The display name
        """
        logging.info('Creating group with name \'{}\'...'
                        .format(displayname))
        data = {
            'displayName': displayname,
            'permissions': [],
            'roleIds': []
        }
        json_data = json.dumps(data, ensure_ascii=False)
        json_data_bytes = json_data.encode('utf-8')
        try:
            req = Request(self._get_creategroup_url(), method='POST')
            for key in self._additionalheaders:
                value = self._additionalheaders[key]
                if value:
                    req.add_header(key, value)
            req.add_header('Content-Type', 'application/json; charset=utf-8')
            req.add_header('Content-Length', len(json_data_bytes))
            with urlopen(req, json_data_bytes) as request:
                return (True, request.read().decode('utf-8'))
        except HTTPError as exception:
            return (False, json_data, exception)

    def _create_role(self, displayname):
        """Creates a new role in COYO

        :param displayname: The display name
        """
        logging.info('Creating role with name \'{}\'...'
                        .format(displayname))
        data = {
            'displayName': displayname,
            'permissions': [],
            'groupIds': []
        }
        json_data = json.dumps(data, ensure_ascii=False)
        json_data_bytes = json_data.encode('utf-8')
        try:
            req = Request(self._get_createrole_url(), method='POST')
            for key in self._additionalheaders:
                value = self._additionalheaders[key]
                if value:
                    req.add_header(key, value)
            req.add_header('Content-Type', 'application/json; charset=utf-8')
            req.add_header('Content-Length', len(json_data_bytes))
            with urlopen(req, json_data_bytes) as request:
                return (True, request.read().decode('utf-8'))
        except HTTPError as exception:
            return (False, json_data, exception)

    def _get_mappedgroupids(self, data_dict, jsondata={}):
        """Maps the given group names/ids to valid groups.
           Creates groups if necessary

        :param data_dict: The data as a dictionary
        """
        new_groupids = []
        if 'groupIds' in data_dict:
            for groupid in data_dict['groupIds']:
                foundgroup = False
                for sgid, sgval in self.groups.items():
                    if groupid == sgid or groupid == sgval['displayName']:
                        logging.debug('Found existing group with ID \'{}\'.'
                                        .format(sgid))
                        new_groupids.append(sgid)
                        foundgroup = True
                        break
                if not foundgroup:
                    logging.debug('Found non-existing group \'{}\'.'
                                    .format(groupid))
                    groupresponse = self._create_group(groupid)
                    if groupresponse[0]:
                        parsedjson = json.loads(groupresponse[1])
                        new_groupids.append(parsedjson['id'])
                        self.groups[parsedjson['id']] = parsedjson
        if not self.appconfig.override_groupdata and 'groups' in jsondata:
            logging.debug('Adding {} existing groups'
                            .format(len(jsondata['groups'])))
            for group in jsondata['groups']:
                if not group['id'] in new_groupids:
                    new_groupids.append(group['id'])
        return new_groupids

    def _get_mappedroleids(self, data_dict, jsondata={}):
        """Maps the given role names/ids to valid groups.
           Creates roles if necessary

        :param data_dict: The data as a dictionary
        """
        new_roleids = []
        if 'roleIds' in data_dict:
            for roleid in data_dict['roleIds']:
                foundrole = False
                for sgid, sgval in self.roles.items():
                    if roleid == sgid or roleid == sgval['displayName']:
                        logging.debug('Found existing role with ID \'{}\''.format(sgid))
                        new_roleids.append(sgid)
                        foundrole = True
                        break
                if not foundrole:
                    logging.debug('Found non-existing role \'{}\''
                                    .format(roleid))
                    roleresponse = self._create_role(roleid)
                    if roleresponse[0]:
                        parsedjson = json.loads(roleresponse[1])
                        new_roleids.append(parsedjson['id'])
                        self.roles[parsedjson['id']] = parsedjson
        if not self.appconfig.override_roledata and 'roles' in jsondata:
            logging.debug('Adding {} existing roles'
                            .format(len(jsondata['roles'])))
            for role in jsondata['roles']:
                if not role['id'] in new_roleids:
                    new_roleids.append(role['id'])
        return new_roleids

    def _update_user(self, data_dict, jsondata):
        """Updates a new user in COYO

        :param data_dict: The data as a dictionary
        """
        assert isinstance(data_dict, dict)

        data_dict['groupIds'] = self._get_mappedgroupids(data_dict, jsondata)
        data_dict['roleIds'] = self._get_mappedroleids(data_dict, jsondata)

        data_dict['slug'] = jsondata['slug']
        json_data = json.dumps(data_dict, ensure_ascii=False)
        json_data_bytes = json_data.encode('utf-8')

        try:
            req = Request(self._get_updateuser_url(jsondata['id']), method='PUT')
            for key in self._additionalheaders:
                value = self._additionalheaders[key]
                if value:
                    req.add_header(key, value)
            req.add_header('Content-Type', 'application/json; charset=utf-8')
            req.add_header('Content-Length', len(json_data_bytes))
            with urlopen(req, json_data_bytes) as request:
                # logging.debug('Successfully sent user data to COYO.')
                return (True, request.read().decode('utf-8'))
        except HTTPError as exception:
            return (False, json_data, exception)

    def _create_user(self, data_dict):
        """Creates a new user in COYO

        :param data_dict: The data as a dictionary
        """
        assert isinstance(data_dict, dict)

        # Mail must not be null
        data_dict['email'] = data_dict['email'] if data_dict['email'] else ''
        data_dict['groupIds'] = self._get_mappedgroupids(data_dict)
        data_dict['roleIds'] = self._get_mappedroleids(data_dict)

        json_data = json.dumps(data_dict, ensure_ascii=False)
        json_data_bytes = json_data.encode('utf-8')
        try:
            req = Request(self._get_createuser_url(), method='POST')
            for key in self._additionalheaders:
                value = self._additionalheaders[key]
                if value:
                    req.add_header(key, value)
            req.add_header('Content-Type', 'application/json; charset=utf-8')
            req.add_header('Content-Length', len(json_data_bytes))
            with urlopen(req, json_data_bytes) as request:
                return (True, request.read().decode('utf-8'))
        except HTTPError as exception:
            return (False, json_data, exception)
