#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright 2017-2018 Denis Meyer
#
# This file is part of the COYO language key importer.
#

"""CoyoData - AppConfig"""

from helper.Constants import DEFAULT_ROW_MAP

class AppConfig:
    """The application configuration"""

    def __init__(self,
                 csv_separator=',',
                 array_separator='%',
                 override_groupdata=True,
                 override_roledata=True,
                 row_map=DEFAULT_ROW_MAP):
        """Initialization"""
        self.csv_separator = csv_separator
        self.array_separator = array_separator
        self.override_groupdata = override_groupdata
        self.override_roledata = override_roledata
        self.row_map = row_map

        self.appname = 'COYO User Importer'
        self.author = 'Denis Meyer'
        self.version = '1.0.0'
        self.copyright = '© 2018 Denis Meyer'
        self.img_logo_app = None
