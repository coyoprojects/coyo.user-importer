#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright 2017-2018 Denis Meyer
#
# This file is part of the COYO User Importer.
#

"""COYO User Importer - CoyoCsvUserImporter"""

import logging
import math
from datetime import datetime

from service.AppConfig import AppConfig
from service.coyo.CoyoData import CoyoData
from service.coyo.CoyoOps import CoyoOps
from csvtools.CsvJsonConverter import CsvJsonConverter

class CoyoCsvUserImporter:
    """Reads in a CSV file and creates the users in a COYO instance"""

    def __init__(self,
                 appconfig,
                 coyodata,
                 csv_input_path,
                 log=print):
        """Initializes the CoyoCsvUserImporter

        :param appconfig: The AppConfig
        :param coyodata: The CoyoData
        :param csv_input_path: The CSV input path
        :param log: The UI logger
        """
        self.appconfig = appconfig
        self.coyodata = coyodata
        self.csv_input_path = csv_input_path
        self.log = log

        self.processed = False
        self.failedusers = []
        self._dfsize = 0

        self.ops = CoyoOps(self.appconfig, self.coyodata)

    def importusers(self):
        """Reads the CSV file and imports users into COYO"""
        logging.debug('Import users')
        self.failedusers = []
        self.processed = False
        self.ops.init()
        if self.ops.accesstoken:
            csvjsonconverter = CsvJsonConverter(appconfig=self.appconfig,
                                                csv_input_path=self.csv_input_path)
            csvjsonconverter.initcsv()
            self._dfsize = csvjsonconverter.df.shape[0]
            logging.info('Processing users...')
            self.log('Processing users...')
            starttime = datetime.now()
            rowsstarttime = datetime.now()
            printmod = 1
            if self._dfsize in range(50, 200):
                printmod = 5
            elif self._dfsize >= 200:
                printmod = 10
            for index, row in csvjsonconverter.df.iterrows():
                row_repr = csvjsonconverter.map_row_to_repr(row)
                if index == 0 or index == self._dfsize - 1 or index % printmod == 0:
                    fulldelta = datetime.now() - starttime
                    rowsdelta = datetime.now() - rowsstarttime
                    percent = math.floor((index + 1) / self._dfsize * 100)
                    rows = rowsdelta.total_seconds()
                    fds = fulldelta.total_seconds()
                    logging.info('Status: {:3}% - {:5} of {:5} users in {:4.4f}s/{:4.4f}s'
                        .format(percent, (index + 1), self._dfsize, rows, fds))
                    self.log('Status: {:3}% - {:5} of {:5} users in {:4.4f}s/{:4.4f}s'
                        .format(percent, (index + 1), self._dfsize, rows, fds))
                    rowsstarttime = datetime.now()
                usercreateresponse = self.ops.createorupdate_user(row_repr)
                if not usercreateresponse[0]:
                    logging.error('Could not create or update user: {}'.format(usercreateresponse[2]))
                    #self.log('Could not create or update user: {}'.format(usercreateresponse[2]))
                    self.failedusers.append(usercreateresponse[1])
            delta = datetime.now() - starttime
            logging.info('Done processing {} users in {:4.4f} seconds.'.format(self._dfsize, delta.total_seconds()))
            self.log('Done processing {} users in {:4.4f} seconds.'.format(self._dfsize, delta.total_seconds()))
            self.processed = True
            return self.failedusers
        else:
            logging.info('Cannot process users without a valid access token.')
            self.log('Cannot process users without a valid access token.')

    def print_failedusers_info(self):
        """Prints information about failed users"""
        logging.debug('Print failed users info')
        if self.processed and self.failedusers:
            logging.info('Failed to import {}/{} users.'.format(len(self.failedusers), self._dfsize))
            self.log('Failed to import {}/{} users.'.format(len(self.failedusers), self._dfsize))
        elif self.processed:
            logging.info('No users failed to be imported.')
            self.log('No users failed to be imported.')

    def print_failedusers(self):
        """Prints all failed users"""
        logging.debug('Print failed users')
        if self.processed and self.failedusers:
            logging.info('{\n\t"failedusers":[')
            self.log('{\n\t"failedusers":[')
            for index, user in enumerate(self.failedusers):
                logging.info('\t\t{}{}'.format(user, ',' if index < len(self.failedusers) - 1 else ''))
                self.log('\t\t{}{}'.format(user, ',' if index < len(self.failedusers) - 1 else ''))
            logging.info('\t]\n}')
            self.log('\t]\n}')
