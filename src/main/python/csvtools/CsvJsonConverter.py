#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright 2017-2018 Denis Meyer
#
# This file is part of the COYO User Importer.
#

"""COYO User Importer - CsvJsonConverter"""

import logging
from pathlib import Path

import pandas as pd

from service.AppConfig import AppConfig

class CsvJsonConverter:
    """Converts csv data to COYO-compatible JSON"""

    def __init__(self,
                 appconfig,
                 csv_input_path=''):
        """Initializes the CsvJsonConverter

        :param appconfig: The AppConfig
        :param csv_input_path: The CSV input path
        """
        self.appconfig = appconfig
        self.csv_input_path = csv_input_path

        self.df = None
        self.dfjson = None

    def initcsv(self):
        """Initializes the CSV"""
        logging.debug('Initializing CSV')
        self._convert_csv_to_dataframe()
        self._convert_csv_to_json()

    def _assert_is_file(self):
        """Asserts that the input CSV file is valid"""
        assert Path(self.csv_input_path).is_file(), 'Given input \'{}\' is not a valid file'.format(self.csv_input_path)

    def _convert_csv_to_dataframe(self):
        """Reads in a CSV file and converts it into a DataFrame."""
        logging.debug('Converting CSV to dataframe')
        self._assert_is_file()
        logging.debug('Reading from \'{}\'.'.format(self.csv_input_path))
        self.df = pd.read_csv(self.csv_input_path, sep=self.appconfig.csv_separator)

    def _convert_csv_to_json(self):
        """Converts a DataFrame to JSON."""
        logging.debug('Converting CSV to JSON.')
        self.dfjson = self.df.to_json(orient='records')

    def map_row_to_repr(self, row):
        """Maps a row to the desired JSON representation.
        
        :param row: The row
        """
        # logging.debug('Mapping row to the required representation for COYO.')
        data = {}
        for k, v in self.appconfig.row_map.items():
            if v[0] in row:
                datap = row[v[0]]
                if isinstance(v[1], bool):
                    dl = row[v[0]]#.lower()
                    datap = True if (dl or dl == 'True' or dl == 'true' or dl == '1') else False
                if isinstance(v[1], list):
                    cdata = []
                    if datap.startswith('[') and datap.endswith(']'):
                        tmpstr = datap[1:-1]
                        for comp in tmpstr.split(self.appconfig.array_separator):
                            if comp.startswith('\'') and comp.endswith('\''):
                                cdata.append(comp[1:-1])
                            elif comp.startswith('\''):
                                cdata.append(comp[1:])
                            elif comp.endswith('\''):
                                cdata.append(comp[:-1])
                            else:
                                cdata.append(comp)
                    datap = cdata
                data[k] = datap
            elif isinstance(v[1], bool):
                data[k] = v[1]
            elif isinstance(v[1], list):
                data[k] = []
            elif isinstance(v[1], str):
                data[k] = None
            else:
                data[k] = v[0]

        return data
